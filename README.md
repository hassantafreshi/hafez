#  Fal Hafez Shirazi
### The Hafez Poetry Randomizer is a delightful WordPress plugin that brings the enchanting world of Hafez's poetry to your website. With just a simple shortcode, you can easily display random verses of Hafez's timeless poetry to your visitors, adding a touch of cultural elegance to your site's content.
<div align="center">
    <a href="https://github.com/hassantafreshi/WP-Fal-Hafez">
        Hafez WP plugin
    </a>
</div>

<div align="center">
  <a href="https://yerib.com/wp-plugins/hafez/">
       Demo of Hafez WP plugin
    </a>
</div>

<div align="center">
    Fal Hafez Shirazi  created by
    <a href="https://yerib.com/">
       Yerib
    </a>
</div>


     
